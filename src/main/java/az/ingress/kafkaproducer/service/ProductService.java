package az.ingress.kafkaproducer.service;

import az.ingress.kafkaproducer.dto.ProductDto;

public interface ProductService {
    String sendMessageAsync(ProductDto productDto);

    String sendMessageSync(ProductDto productDto) throws Exception;


}
